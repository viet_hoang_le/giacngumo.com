<style xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">

    #contact_parallax{
        background: url('../../images/parallax/contact_bg.png') 50% 0 no-repeat fixed;
        width: 940px;
        height: 1040px;
        margin: 0 auto;
        padding: 0;
    }
    #info_parallax{
        background: url('../../images/parallax/info_bg.png') 50% 0 no-repeat fixed;
        width: 940px;
        height: 1040px;
        margin: 0 auto;
        padding: 0;
    }
    #address_parallax{
        width: 940px;
        height: 1040px;
        margin: 0 auto;
        padding: 0;
    }
    #view_1{
        background: url('../../images/parallax/viet_bg.png') 50% 0 no-repeat fixed;
        width: 940px;
        height: 1040px;
        margin: 0 auto;
        padding: 0;
    }
    #view_2{
        background: url(images/secondBG.jpg) 50% 0 no-repeat fixed;
        height: 400px;
        margin: 0 auto;
        padding: 40px 0 0 0;
    }
    #view_3{
        background: url(images/thirdBG.jpg) 50% 0 no-repeat fixed;
        height: 400px;
        margin: 0 auto;
        padding: 40px 0 0 0;
    }
</style>

<?php
/* @var $this ContactController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Contacts',
);

if($validUser)
    $this->menu=array(
        array('label'=>'Create Contact', 'url'=>array('create')),
        array('label'=>'Manage Contact', 'url'=>array('admin')),
    );
?>


<head>
<script type="text/javascript">
    $(document).ready(function(){
        $('#nav').localScroll(800);

        //.parallax(xPosition, speedFactor, outerHeight) options:
        //xPosition - Horizontal position of the element
        //inertia - speed to move relative to vertical scroll. Example: 0.1 is one tenth the speed of scrolling, 2 is twice the speed of scrolling
        //outerHeight (true/false) - Whether or not jQuery should use it's outerHeight option to determine when a section is in the viewport
        $('#contact_parallax').parallax("50%", 0.1);
        $('#info_parallax').parallax("50%", 0.1);
        $('#address_parallax').parallax("50%", 0.4);
        $('#view_1').parallax("50%", 0.3);
    })
</script>
</head>

<body>
    <ul id="nav">
        <li><a href="#contact_parallax" title="Next Section"></a></li>
        <li><a href="#info_parallax" title="Next Section"></a></li>
        <li><a href="#address_parallax" title="Next Section"></a></li>
        <li><a href="#view_1" title="Next Section"></a></li>
    </ul>

<div id="contact_parallax">
    <div class="story">
        <div class="float-left">

            <?php if(Yii::app()->user->hasFlash('contact')): ?>

            <div class="flash-success">
                <?php echo Yii::app()->user->getFlash('contact'); ?>
            </div>

            <?php else: ?>

            <p>
                If you have business inquiries or other questions, please fill out the following form to contact us. This form below will send an email directly to the founders and other related people. Thank you.
            </p>

            <div class="form">

                <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'contact-form',
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                ),
            )); ?>

                <p class="note">Fields with <span class="required">*</span> are required.</p>

                <?php echo $form->errorSummary($model); ?>

                <div class="row">
                    <?php echo $form->labelEx($model,'name'); ?>
                    <?php echo $form->textField($model,'name'); ?>
                    <?php echo $form->error($model,'name'); ?>
                </div>

                <div class="row">
                    <?php echo $form->labelEx($model,'email'); ?>
                    <?php echo $form->textField($model,'email'); ?>
                    <?php echo $form->error($model,'email'); ?>
                </div>

                <div class="row">
                    <?php echo $form->labelEx($model,'subject'); ?>
                    <?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>128)); ?>
                    <?php echo $form->error($model,'subject'); ?>
                </div>

                <div class="row">
                    <?php echo $form->labelEx($model,'body'); ?>
                    <?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50)); ?>
                    <?php echo $form->error($model,'body'); ?>
                </div>

                <?php if(CCaptcha::checkRequirements()): ?>
                <div class="row">
                    <?php echo $form->labelEx($model,'verifyCode'); ?>
                    <div>
                        <?php $this->widget('CCaptcha'); ?>
                        <?php echo $form->textField($model,'verifyCode'); ?>
                    </div>
                    <div class="hint">Please enter the letters as they are shown in the image above.
                        <br/>Letters are not case-sensitive.</div>
                    <?php echo $form->error($model,'verifyCode'); ?>
                </div>
                <?php endif; ?>

                <div class="row buttons">
                    <?php echo CHtml::submitButton('Submit'); ?>
                </div>

                <?php $this->endWidget(); ?>

            </div><!-- form -->

            <?php endif; ?>
        </div>
    </div> <!--.story-->
</div> <!--#contact_bg-->

<div id="info_parallax">
    <div class="story">
        <div class="float-right">
            <p>
                <h1>Contact Information</h1>
            </p>
            <p>E-mail:  info@giacngumo.com</p>
            <p>Address: Hochiminh city, Vietnam</p>
            <p>Cell Phone: (+84) 987 320 997</p>
        </div>
    </div> <!--.story-->

</div> <!--#info_parallax-->

<div id="address_parallax">
    <iframe width="940" height="1040" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=+&amp;q=hochiminh+city&amp;ie=UTF8&amp;hq=&amp;hnear=Ho+Chi+Minh+City,+Vietnam&amp;ll=10.822515,106.347656&amp;spn=1.135331,1.929474&amp;t=m&amp;z=10&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=+&amp;q=hochiminh+city&amp;ie=UTF8&amp;hq=&amp;hnear=Ho+Chi+Minh+City,+Vietnam&amp;ll=10.822515,106.347656&amp;spn=1.135331,1.929474&amp;t=m&amp;z=10" style="color:#0000FF;text-align:left">View Larger Map</a></small>
</div> <!--#address_parallax-->

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider,
    'itemView'=>'_view',
    'summaryText' => '',
)); ?>

</body>