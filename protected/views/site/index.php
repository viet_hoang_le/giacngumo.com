<head>

    <!-- FlexSlider -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/../../FlexSlider/flexslider.css" type="text/css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/../../FlexSlider/jquery.flexslider.js"></script>
    <!-- Place in the <head>, after the three links -->
    <script type="text/javascript" charset="utf-8">
        $(window).load(function() {
            $('.flexslider').flexslider({
                directionNav: false
            });
        });
    </script>

    <style>
        #div-outerSlide {
            position:relative;
            top:0;
            left:0;
            width:100%;
        }
    </style>
</head>
<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

Yii::app()->theme = 'jumpforjoy_1';
?>
<br/>
<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>
    <br/>

<div class="flexslider" id="div-outerSlide">
    <ul class="slides">
        <li>
            <img src="<?php echo Yii::app()->theme->baseUrl ?>/../../images/slides/Software slide.png">
        </li>
        <li>
            <img src="<?php echo Yii::app()->theme->baseUrl ?>/../../images/slides/Design slide.png">
        </li>
        <li>
            <img src="<?php echo Yii::app()->theme->baseUrl ?>/../../images/slides/Mobile slide.png">
        </li>
        <li>
            <img src="<?php echo Yii::app()->theme->baseUrl ?>/../../images/slides/Addon slide.png">
        </li>
        <li>
            <img src="<?php echo Yii::app()->theme->baseUrl ?>/../../images/slides/Maintenance slide.png">
        </li>
        <li>
            <img src="<?php echo Yii::app()->theme->baseUrl ?>/../../images/slides/Smart Solution.png">
        </li>
    </ul>
</div>