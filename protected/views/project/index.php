<?php
/* @var $this ProjectController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Projects',
);

if($validUser)
    $this->menu=array(
        array('label'=>'Create Project', 'url'=>array('create')),
        array('label'=>'Manage Project', 'url'=>array('admin')),
    );
?>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
    'summaryText' => '',
)); ?>
