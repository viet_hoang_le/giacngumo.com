<?php
/* @var $this ProjectController */
/* @var $model Project */

$this->breadcrumbs=array(
	'Projects'=>array('index'),
	$model->id,
);

if($validUser)
    $this->menu=array(
        array('label'=>'List Project', 'url'=>array('index')),
        array('label'=>'Create Project', 'url'=>array('create')),
        array('label'=>'Update Project', 'url'=>array('update', 'id'=>$model->id)),
        array('label'=>'Delete Project', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        array('label'=>'Manage Project', 'url'=>array('admin')),
    );
?>

<h1>Project #<?php echo $model->id; ?></h1>

<?php
/*$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'type',
		'body_title',
		'body_header',
		'body_content',
		'updated_at',
	),
)); */
?>

<p>
    <?php echo $model->body_title; ?>
</p>
<p>
    <?php echo $model->body_header; ?>
</p>
<p>
    <?php echo $model->body_content; ?>
</p>
<p><i>
    type: <?php echo $model->type; ?>
    <br/>
    updated_at: <?php echo $model->updated_at; ?>
</i></p>
