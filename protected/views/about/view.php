<?php
/* @var $this AboutController */
/* @var $model About */

$this->breadcrumbs=array(
	'Abouts'=>array('index'),
	$model->id,
);

if($validUser)
    $this->menu=array(
        array('label'=>'List About', 'url'=>array('index')),
        array('label'=>'Create About', 'url'=>array('create')),
        array('label'=>'Update About', 'url'=>array('update', 'id'=>$model->id)),
        array('label'=>'Delete About', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        array('label'=>'Manage About', 'url'=>array('admin')),
    );
?>

<h1>About #<?php echo $model->id; ?></h1>

<?php
/* $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'type',
		'body_title',
		'body_header',
		'body_content',
		'updated_at',
	),
));*/ ?>
<p>
    <?php echo $model->body_title; ?>
</p>
<p>
    <?php echo $model->body_header; ?>
</p>
<p>
    <?php echo $model->body_content; ?>
</p>
<p><i>
    type: <?php echo $model->type; ?>
    <br/>
    updated_at: <?php echo $model->updated_at; ?>
</i></p>