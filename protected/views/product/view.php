<?php
/* @var $this ProductController */
/* @var $model Product */

$this->breadcrumbs=array(
	'Products'=>array('index'),
	$model->id,
);

if($validUser)
    $this->menu=array(
        array('label'=>'List Product', 'url'=>array('index')),
        array('label'=>'Create Product', 'url'=>array('create')),
        array('label'=>'Update Product', 'url'=>array('update', 'id'=>$model->id)),
        array('label'=>'Delete Product', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        array('label'=>'Manage Product', 'url'=>array('admin')),
    );
    ?>

<?php
/*
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'type',
		'body_title',
		'body_header',
		'body_content',
		'updated_at',
	),
)); */
?>

<p>
     <?php echo $model->body_title; ?>
    <i>updated_at: <?php echo $model->updated_at; ?></i>
</p>
<p>
    <?php echo $model->body_header; ?>
</p>
<p>
    <?php echo $model->body_content; ?>
</p>