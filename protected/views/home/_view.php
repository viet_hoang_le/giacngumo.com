<?php
/* @var $this HomeController */
/* @var $data Home */
?>

<div class="view">

	<?php /*echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('body_title')); ?>:</b>
	<?php /*echo CHtml::encode($data->body_title);*/ ?>
    <?php echo $data->body_title; ?>
    <i style="font-size: 11px">
        <?php echo CHtml::encode($data->getAttributeLabel('updated_at')); ?>:
        <?php echo CHtml::encode($data->updated_at); ?>
    </i>

	<?php /*echo CHtml::encode($data->getAttributeLabel('body_header')); ?>:</b>
	<?php echo CHtml::encode($data->body_header);*/ ?>
    <?php echo $data->body_header; ?>

    <?php /*echo CHtml::encode($data->getAttributeLabel('body_content')); ?>:</b>
	<?php echo CHtml::encode($data->body_content); ?>
    <?php echo $data->body_content;*/ ?>


</div>