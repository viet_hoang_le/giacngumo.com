<head>
</head>

<?php
/* @var $this HomeController */
/* @var $dataProvider CActiveDataProvider */
/*
$this->breadcrumbs=array(
	'Homes',
);

$this->menu=array(
	array('label'=>'Create Home', 'url'=>array('create')),
	array('label'=>'Manage Home', 'url'=>array('admin')),
);*/

if($validUser)
    $this->menu=array(
        array('label'=>'Create Home', 'url'=>array('create')),
        array('label'=>'Manage Home', 'url'=>array('admin')),
    );
?>

<?php
/*
echo Yii::app()->themeManager->basePath;
echo Yii::app()->theme->baseUrl;
/*
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));

*/

$this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$products,
    'itemView'=>'_view',
    'summaryText' => '',
));
?>

<hr style="height:10px;background: #fff url('../../images/hr line/Hr Logo.png') repeat scroll center;border:none;" />

<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$services,
    'itemView'=>'_view',
    'summaryText' => '',
));
?>

<hr style="height:10px;background: #fff url('../../images/hr line/Hr Logo.png') repeat scroll center;border:none;" />

<h2>News</h2>
<?php

$this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$news,
    'itemView'=>'_view',
    'summaryText' => '',
));
?>
