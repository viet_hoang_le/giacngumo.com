<head>
    <script src="../../../ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../../ckfinder/ckfinder.js"></script>
</head>
<?php
/* @var $this HomeController */
/* @var $model Home */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'home-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
        <?php /*echo $form->dropDownList($model,'type', CHtml::listData(Home::model()->findAll(array('group' => 'type')), 'type', 'type')); */?>
        <?php echo $form->textField($model,'type',array('size'=>60,'maxlength'=>256)); ?>
        <?php echo $form->error($model,'type');?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'body_title'); ?>
		<?php echo $form->textArea($model,'body_title'); ?>
		<?php echo $form->error($model,'body_title'); ?>
	</div>
    <script>
        var body_title_editor = CKEDITOR.replace( 'Home_body_title' );
        CKFinder.setupCKEditor( body_title_editor, '/ckfinder' ) ;
    </script>

	<div class="row">
		<?php echo $form->labelEx($model,'body_header'); ?>
		<?php echo $form->textArea($model,'body_header'); ?>
		<?php echo $form->error($model,'body_header'); ?>
	</div>
    <script>
        var body_header_editor = CKEDITOR.replace( 'Home_body_header' );
        CKFinder.setupCKEditor( body_header_editor, '/ckfinder' ) ;
    </script>

	<div class="row">
		<?php echo $form->labelEx($model,'body_content'); ?>
		<?php echo $form->textArea($model,'body_content'); ?>
		<?php echo $form->error($model,'body_content'); ?>
	</div>
    <script>
        var body_content_editor = CKEDITOR.replace( 'Home_body_content' );
        CKFinder.setupCKEditor( body_content_editor, '/ckfinder' ) ;
    </script>

	<div class="row">
		<?php
            /*echo $form->labelEx($model,'updated_at'); ?>
            <?php echo $form->textField($model,'updated_at'); ?>
            <?php echo $form->error($model,'updated_at'); */
            $date = date('Y-m-d H:i:s', time());
            echo $form->textField($model,'updated_at',array('value'=>$date));
        ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->