<?php
/* @var $this ServiceController */
/* @var $model Service */

$this->breadcrumbs=array(
	'Services'=>array('index'),
	$model->id,
);

if($validUser)
    $this->menu=array(
        array('label'=>'List Service', 'url'=>array('index')),
        array('label'=>'Create Service', 'url'=>array('create')),
        array('label'=>'Update Service', 'url'=>array('update', 'id'=>$model->id)),
        array('label'=>'Delete Service', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        array('label'=>'Manage Service', 'url'=>array('admin')),
    );
?>

<h1>Service #<?php echo $model->id; ?></h1>

<?php
/*$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'type',
		'body_title',
		'body_header',
		'body_content',
		'updated_at',
	),
)); */
?>

<p>
    <?php echo $model->body_title; ?>
</p>
<p>
    <?php echo $model->body_header; ?>
</p>
<p>
    <?php echo $model->body_content; ?>
</p>
<p><i>
    type: <?php echo $model->type; ?>
    <br/>
    updated_at: <?php echo $model->updated_at; ?>
</i></p>
