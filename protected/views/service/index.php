<?php
/* @var $this ServiceController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Services',
);

if($validUser)
    $this->menu=array(
        array('label'=>'Create Service', 'url'=>array('create')),
        array('label'=>'Manage Service', 'url'=>array('admin')),
    );
?>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
    'summaryText' => '',
)); ?>
