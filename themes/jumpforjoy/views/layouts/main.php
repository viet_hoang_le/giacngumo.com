<!doctype html>
<html lang="en">
<head>
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
    <meta charset="utf-8" />
    <title>GiacNguMo</title>

    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/css/styles.css" type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" href="print.css" media="print" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]--><!-- Place somewhere in the <head> of your document -->

    <!-- Social icons -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/../../css-social-buttons/social-buttons.css" type="text/css">

    <!-- CKEditor -->
    <script src="../../../ckeditor/ckeditor.js"></script>
    <script src="../../../ckeditor/config.js"></script>
    <script type="text/javascript" src="../../../ckfinder/ckfinder.js"></script>
</head>
<body>
<div id="wrapper"><!-- #wrapper -->

    <header><!-- header -->
        <h1>
            <a href="/index.php">
                <img src="<?php echo Yii::app()->theme->baseUrl ?>/../../images/logo1.png" width="60">
                GiacNguMo
            </a>
        </h1>
        <h2>Dare to Dream and Work to Win</h2>
        <h4>
            The Sleeping Dream Group
        </h4>

        <div style="float: right;">
            <a href="https://www.facebook.com/bibooki" class="sb circle facebook2">
                <span>Facebook</span>
            </a>
            <!--<a href="https://twitter.com/GiacNguMo" class="sb circle twitter2">
                <span>Twitter</span>
            </a>-->
            <a href="https://plus.google.com/b/109539193593720891384/109539193593720891384/posts" class="sb circle gplus">
                <span>Google+</span>
            </a>
            <!--<a href="https://www.linkedin.com/GiacNguMo" class="sb circle linkedin">
                <span>LinkedIn</span>
            </a>-->
        </div>

        <!-- header image -->
        <p></p>
    </header><!-- end of header -->

    <nav><!-- top nav -->
        <div class="menu">
            <?php $this->widget('zii.widgets.CMenu',array(
            'items'=>array(
                array('label'=>'Home',
                    'url'=>array('/home'),
                ),
                array('label'=>'About Us', 'url'=>array('/about')),
                array('label'=>'Products', 'url'=>array('/product')),
                array('label'=>'Services', 'url'=>array('/service')),
                array('label'=>'Projects', 'url'=>array('/project')),
                array('label'=>'Contact Us', 'url'=>array('/contact')),
                array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
            ),
        )); ?>
        </div>
    </nav><!-- end of top nav -->


    <section id="main"><!-- #main content and sidebar area -->
        <section id="container"><!-- #container -->
            <section id="content"><!-- #content -->
	<?php
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Operations',
		));
		$this->widget('zii.widgets.CMenu', array(
			'items'=>$this->menu,
			'htmlOptions'=>array('class'=>'operations'),
		));
		$this->endWidget();
	?>
                <article>
                    <?php echo $content ?>
                </article>
            </section><!-- end of #content -->
        </section><!-- end of #container -->

        <aside id="sidebar"><!-- sidebar -->
            <h3>Our Sites</h3>
            <br/>
            <ul>
                <li><a href="http://bibooki.com">Bibooki</a></li>
                <br/>
                <li><a href="https://itunes.apple.com/us/developer/hoang-viet-le/id1039272300">iOS apps - AppStore</a></li>
                <br/>
                <li><a href="https://play.google.com/store/search?q=com.bibooki.mobile">Android apps - Google Play</a></li>
                <br/>
                <li><a href="http://www.amazon.com/s/ref=bl_sr_mobile-apps?_encoding=UTF8&field-brandtextbin=Giacngumo&node=2350149011">Android apps - Amazon Store</a></li>
                <br/>
                <li><a href="https://addons.mozilla.org/En-us/firefox/addon/count-word-professional/">Firefox Addon -Count Word Profressional</a></li>
                <br/>
                <li><a href="https://chrome.google.com/webstore/detail/panlmfdocmkofhkdcmbcbipednkjibng/publish-accepted">Chrome Extension - GNM Replacer</a></li>
                <br/>
            </ul>

        </aside><!-- end of sidebar -->

    </section><!-- end of #main content and sidebar-->

    <footer>
        <section id="footer-area" style="text-align: center">

            <section id="footer-outer-block">
                    <h3>Copyright &copy; <?php echo date('Y'); ?> by GiacNguMo.</h3>
                    <p>		
		All Rights Reserved.<br/>
		<?php echo 'Powered by Viet.Hoang.Le@giacngumo.com'; ?></p>

            </section><!-- end of #footer-outer-block -->

        </section><!-- end of #footer-area -->
    </footer>

</div><!-- #wrapper -->
<!-- Free template created by http://freehtml5templates.com -->
</body>
</html>