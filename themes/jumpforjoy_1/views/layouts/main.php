<!doctype html>
<html lang="en">
<head>
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
    <meta charset="utf-8" />
    <title>GiacNguMo</title>

    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/css/styles.css" type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" href="print.css" media="print" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
    <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]--><!-- Place somewhere in the <head> of your document -->

    <!-- FlexSlider -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/../../FlexSlider/flexslider.css" type="text/css">
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/../../FlexSlider/jquery.flexslider.js"></script>
    <!-- Place in the <head>, after the three links -->
    <script type="text/javascript" charset="utf-8">
        $(window).load(function() {
            $('.flexslider').flexslider({
                directionNav: false
            });
        });
    </script>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <!-- Parallax -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/../../jQuery-Parallax-scrolldeck/style.css" type="text/css">
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/../../jQuery-Parallax-scrolldeck/scripts/jquery.localscroll-1.2.7-min.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/../../jQuery-Parallax-scrolldeck/scripts/jquery.parallax-1.1.3.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/../../jQuery-Parallax-scrolldeck/scripts/jquery.scrollTo-1.4.3.1.min.js"></script>

    <!-- Social icons -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/../../css-social-buttons/social-buttons.css" type="text/css">

</head>
<body>
<div id="wrapper"><!-- #wrapper -->
    <header><!-- header -->
        <h1>
            <a href="/index.php">
                <img src="<?php echo Yii::app()->theme->baseUrl ?>/../../images/logo1.png" width="60">
                GiacNguMo
            </a>
        </h1>
        <h2>Dare to Dream and Work to Win</h2>
        <h4>
            The Sleeping Dream Group
        </h4>

        <div style="float: right;">
            <a href="https://www.facebook.com/bibooki" class="sb circle facebook2">
                <span>Facebook</span>
            </a>
            <!--<a href="https://twitter.com/GiacNguMo" class="sb circle twitter2">
                <span>Twitter</span>
            </a>-->
            <a href="https://plus.google.com/b/109539193593720891384/109539193593720891384/posts" class="sb circle gplus">
                <span>Google+</span>
            </a>
            <!--<a href="https://www.linkedin.com/GiacNguMo" class="sb circle linkedin">
                <span>LinkedIn</span>
            </a>-->
        </div>

        <!-- header image -->
        <p></p>
    </header><!-- end of header -->

    <nav><!-- top nav -->
        <div class="menu">
            <?php $this->widget('zii.widgets.CMenu',array(
            'items'=>array(
                array('label'=>'Home',
                    'url'=>array('/home'),
                ),
                array('label'=>'About Us', 'url'=>array('/about')),
                array('label'=>'Products', 'url'=>array('/product')),
                array('label'=>'Services', 'url'=>array('/service')),
                array('label'=>'Projects', 'url'=>array('/project')),
                array('label'=>'Contact Us', 'url'=>array('/contact')),
                array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
            ),
        )); ?>
        </div>
    </nav><!-- end of top nav -->

        <section id="container"><!-- #container -->
	<?php
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Operations',
		));
		$this->widget('zii.widgets.CMenu', array(
			'items'=>$this->menu,
			'htmlOptions'=>array('class'=>'operations'),
		));
		$this->endWidget();
	?>
                    <?php echo $content ?>
        </section><!-- end of #container -->

    <footer>
        <section id="footer-area" style="text-align: center">

            <section id="footer-outer-block">
                    <h3>Copyright &copy; <?php echo date('Y'); ?> by GiacNguMo.</h3>
                    <p>		
		All Rights Reserved.<br/>
		<?php echo 'Powered by Viet.Hoang.Le@giacngumo.com'; ?></p>

            </section><!-- end of #footer-outer-block -->

        </section><!-- end of #footer-area -->
    </footer>

</div><!-- #wrapper -->
<!-- Free template created by http://freehtml5templates.com -->
</body>
</html>